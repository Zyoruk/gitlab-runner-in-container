#!/bin/bash
# usage ./register.sh <token>

# In order to run you have to export the token
export TOKEN=$1

docker-compose -f docker-compose.register.yml up